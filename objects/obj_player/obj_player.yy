{
    "id": "58501f2a-9545-4728-b4bd-dcf014f756ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "ae24cb1d-c4ae-4fec-b1c7-a95d7d1ba0de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "58501f2a-9545-4728-b4bd-dcf014f756ea"
        },
        {
            "id": "da4ace55-2d58-429f-9d8d-056575c3e9d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "58501f2a-9545-4728-b4bd-dcf014f756ea"
        }
    ],
    "maskSpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
    "visible": true
}