{
    "id": "af85ef56-2172-4e95-b8ff-e8d56d98abd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arms",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67d756a1-eae3-4256-8f30-218d556cfcd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af85ef56-2172-4e95-b8ff-e8d56d98abd6",
            "compositeImage": {
                "id": "52b39f39-880a-4210-b206-1ac0229cb899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d756a1-eae3-4256-8f30-218d556cfcd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d752d4d-09bf-4dde-abf4-1f3997a9cb53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d756a1-eae3-4256-8f30-218d556cfcd4",
                    "LayerId": "14ffa092-ffc6-41a5-b3b5-df2630d37bfc"
                }
            ]
        },
        {
            "id": "3b7972a9-bc85-4d6e-ade5-eb7243065684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af85ef56-2172-4e95-b8ff-e8d56d98abd6",
            "compositeImage": {
                "id": "12b070c0-41d9-4375-ae05-27f051cce6fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7972a9-bc85-4d6e-ade5-eb7243065684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ebb745-5d40-4de7-9e64-b80c100d3b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7972a9-bc85-4d6e-ade5-eb7243065684",
                    "LayerId": "14ffa092-ffc6-41a5-b3b5-df2630d37bfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "14ffa092-ffc6-41a5-b3b5-df2630d37bfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af85ef56-2172-4e95-b8ff-e8d56d98abd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 1,
    "yorig": 2
}