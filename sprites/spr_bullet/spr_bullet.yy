{
    "id": "9c774435-60d7-4156-8879-ac60329e03a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1df969af-adf1-4e39-9213-a7ffa5091967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c774435-60d7-4156-8879-ac60329e03a4",
            "compositeImage": {
                "id": "282e96f7-3752-4959-ac50-0239b8578710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df969af-adf1-4e39-9213-a7ffa5091967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41f53ac4-5084-4c50-9643-ac46af39d20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df969af-adf1-4e39-9213-a7ffa5091967",
                    "LayerId": "295a050c-9b2d-4adf-86c8-d5e72ff7ee2a"
                }
            ]
        },
        {
            "id": "c36ef4a1-883b-4645-a480-aa6a47df467a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c774435-60d7-4156-8879-ac60329e03a4",
            "compositeImage": {
                "id": "d5905302-6adf-400b-92e1-528802c9b82e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c36ef4a1-883b-4645-a480-aa6a47df467a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab761179-7029-4aab-80db-8eab0005fc94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c36ef4a1-883b-4645-a480-aa6a47df467a",
                    "LayerId": "295a050c-9b2d-4adf-86c8-d5e72ff7ee2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "295a050c-9b2d-4adf-86c8-d5e72ff7ee2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c774435-60d7-4156-8879-ac60329e03a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 6,
    "yorig": 9
}