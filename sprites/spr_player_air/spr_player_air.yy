{
    "id": "b47061cd-9b7b-45f7-8035-268dad3a9a86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_air",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bbcef98-5069-4030-afca-0406f4af1772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b47061cd-9b7b-45f7-8035-268dad3a9a86",
            "compositeImage": {
                "id": "0980e93a-0720-4d43-b3fe-071c9545e35e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbcef98-5069-4030-afca-0406f4af1772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89f12752-cd69-4e74-9a75-04587c0cc230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbcef98-5069-4030-afca-0406f4af1772",
                    "LayerId": "04647ace-d5cc-4096-95c6-696e4cda2682"
                }
            ]
        },
        {
            "id": "f5581382-6334-4ce6-a006-a2eae2201dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b47061cd-9b7b-45f7-8035-268dad3a9a86",
            "compositeImage": {
                "id": "5ba13855-7940-4ed0-aaad-d819e22cce67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5581382-6334-4ce6-a006-a2eae2201dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36304ea2-375b-49ba-bd4a-ff6cdecf72f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5581382-6334-4ce6-a006-a2eae2201dba",
                    "LayerId": "04647ace-d5cc-4096-95c6-696e4cda2682"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04647ace-d5cc-4096-95c6-696e4cda2682",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b47061cd-9b7b-45f7-8035-268dad3a9a86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 16
}