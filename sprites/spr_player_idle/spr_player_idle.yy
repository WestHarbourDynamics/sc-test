{
    "id": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5876f467-69a6-4160-aa7c-895f03192cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "compositeImage": {
                "id": "865155ac-cf03-4c1c-8063-7fea3661cd55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5876f467-69a6-4160-aa7c-895f03192cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da8c5b8-c724-4fca-a2a2-98dd933ddc9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5876f467-69a6-4160-aa7c-895f03192cd3",
                    "LayerId": "f398de65-f81d-4c19-82aa-50814c8e8d08"
                }
            ]
        },
        {
            "id": "0682f399-2485-4fef-bd7d-074788e5732b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "compositeImage": {
                "id": "98a2d583-66ad-4c23-8a7e-a8a2e46921ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0682f399-2485-4fef-bd7d-074788e5732b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb992d9-ea61-4fed-b839-6a6860c5ebe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0682f399-2485-4fef-bd7d-074788e5732b",
                    "LayerId": "f398de65-f81d-4c19-82aa-50814c8e8d08"
                }
            ]
        },
        {
            "id": "72bf862c-ab02-4d96-b935-9993d387ee5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "compositeImage": {
                "id": "e60f9442-34f0-473b-9ace-4445fb9f50cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72bf862c-ab02-4d96-b935-9993d387ee5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489a7bd0-e413-4819-b3eb-525236c2076c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72bf862c-ab02-4d96-b935-9993d387ee5f",
                    "LayerId": "f398de65-f81d-4c19-82aa-50814c8e8d08"
                }
            ]
        },
        {
            "id": "81020e7c-d989-4159-9349-7ea0718b8bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "compositeImage": {
                "id": "441eb8b8-77b1-410d-948d-ac3387258f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81020e7c-d989-4159-9349-7ea0718b8bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8269ebcb-1159-4f1e-a005-0f8e77860789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81020e7c-d989-4159-9349-7ea0718b8bb2",
                    "LayerId": "f398de65-f81d-4c19-82aa-50814c8e8d08"
                }
            ]
        },
        {
            "id": "e53bd69b-7bcd-44ea-b7b0-efa84e3bf9bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "compositeImage": {
                "id": "991ecdbe-7113-47cc-a4c1-5131926f968e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e53bd69b-7bcd-44ea-b7b0-efa84e3bf9bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3a30a6-3ba0-40c4-b851-185af28cd300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e53bd69b-7bcd-44ea-b7b0-efa84e3bf9bf",
                    "LayerId": "f398de65-f81d-4c19-82aa-50814c8e8d08"
                }
            ]
        },
        {
            "id": "11f15873-c0be-43c6-a5fa-db5b6c92827a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "compositeImage": {
                "id": "a6db012a-feec-4a9d-a64d-8aab1c4d6ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11f15873-c0be-43c6-a5fa-db5b6c92827a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83955e85-b062-44a6-89b6-a0a9ceba65c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11f15873-c0be-43c6-a5fa-db5b6c92827a",
                    "LayerId": "f398de65-f81d-4c19-82aa-50814c8e8d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f398de65-f81d-4c19-82aa-50814c8e8d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63b3d39d-415c-4ae6-bb20-667a6f5c113b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 16
}