{
    "id": "c299c6a0-65b3-436e-952d-5949973e0270",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89d7e605-6188-4894-b5a3-d2cc23df0e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c299c6a0-65b3-436e-952d-5949973e0270",
            "compositeImage": {
                "id": "b2300ee8-e1bc-459c-8822-1dc89c20ee7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d7e605-6188-4894-b5a3-d2cc23df0e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c998561-87a2-4543-92ff-180af499a316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d7e605-6188-4894-b5a3-d2cc23df0e7e",
                    "LayerId": "90a9271e-3a64-47ea-9c92-f6cf79c4fa6a"
                },
                {
                    "id": "7aa6da4d-da4e-4efb-a22e-3e147e77ac51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d7e605-6188-4894-b5a3-d2cc23df0e7e",
                    "LayerId": "c4b54a83-08d3-472e-8bb0-2ea92e1dbb3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c4b54a83-08d3-472e-8bb0-2ea92e1dbb3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c299c6a0-65b3-436e-952d-5949973e0270",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "90a9271e-3a64-47ea-9c92-f6cf79c4fa6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c299c6a0-65b3-436e-952d-5949973e0270",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 16
}