{
    "id": "60ad6dbe-b636-4843-a69e-f7719de521aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 13,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff4ccc90-e595-4544-a734-944a8130db87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "590bdd48-c643-4f0c-accb-fea064f37e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff4ccc90-e595-4544-a734-944a8130db87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4501e5a3-04f4-4f4f-ab20-a60c108a3045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff4ccc90-e595-4544-a734-944a8130db87",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "73600177-8ca8-4336-bb33-e487932463e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "f12ee2af-701e-42d6-9474-397fe556d202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73600177-8ca8-4336-bb33-e487932463e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7036b6d6-315e-495e-85b5-a38cce1f40fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73600177-8ca8-4336-bb33-e487932463e5",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "1dfc822e-bf51-438a-8a8e-6666a9dcc864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "41f619bd-48b9-4357-9859-90539ee1e647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfc822e-bf51-438a-8a8e-6666a9dcc864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766cdce5-cd83-46ba-a1f3-3434ac7f025c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfc822e-bf51-438a-8a8e-6666a9dcc864",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "b197c57f-8d0c-4039-a2ca-0d29b5393038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "381ce1b4-4e40-43e1-a66b-e988f0d84487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b197c57f-8d0c-4039-a2ca-0d29b5393038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8181a9c7-a39c-47aa-8db2-8b747a6861c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b197c57f-8d0c-4039-a2ca-0d29b5393038",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "22dbdb24-3659-44ee-a6a2-3f6337aa8a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "a9c25099-ccd4-4adf-b27a-b3c199851019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22dbdb24-3659-44ee-a6a2-3f6337aa8a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148cf0e0-9080-4692-9434-9039deccfc4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22dbdb24-3659-44ee-a6a2-3f6337aa8a14",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "49d0ee2b-0ba1-4b2e-91fe-03583833646a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "8ab09db9-1c4a-4cf1-94f6-347b8a7487fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49d0ee2b-0ba1-4b2e-91fe-03583833646a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382382d0-b00c-4823-bf72-3fc8119b50e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49d0ee2b-0ba1-4b2e-91fe-03583833646a",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "7ac60a2a-4937-4b87-9575-a167e322a7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "133bd47f-3556-4321-a50b-a43c489ff211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac60a2a-4937-4b87-9575-a167e322a7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb36d21-8ddb-4e5f-9806-42e7392fc769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac60a2a-4937-4b87-9575-a167e322a7ea",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "1db64ef6-4c93-4b69-8efb-315225f28886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "aa81891f-fd40-4e7b-b028-20e849a54ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db64ef6-4c93-4b69-8efb-315225f28886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373b1b4d-cb47-410c-83c0-a1bc09533af7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db64ef6-4c93-4b69-8efb-315225f28886",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "76c4cc44-f284-4975-909e-99a93dcf1830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "b1338043-5e7d-4024-a500-2e6ffc78d9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c4cc44-f284-4975-909e-99a93dcf1830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde80e2f-38c0-4b03-aa94-d1069e4070a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c4cc44-f284-4975-909e-99a93dcf1830",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "28a2334c-e7ca-4e5f-81c7-9f778b44342c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "11c9e54d-9c96-4d45-94e4-25b5cb9a1a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a2334c-e7ca-4e5f-81c7-9f778b44342c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bceea13-cfca-47c1-bb97-c2faa4949ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a2334c-e7ca-4e5f-81c7-9f778b44342c",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "40e00353-da4e-41fb-9a8d-7e46c10a7dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "3357c5d4-1ebc-45a5-a4df-63e7c329b908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40e00353-da4e-41fb-9a8d-7e46c10a7dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d030443d-9de9-4b99-93f4-5a6d123738c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40e00353-da4e-41fb-9a8d-7e46c10a7dd5",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "27ea1e62-a128-4139-a4f1-c9435edbf374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "0e22d7a0-267f-4333-900e-95bb66688d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ea1e62-a128-4139-a4f1-c9435edbf374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef36765d-b88e-47db-a3e8-d1bd670fd1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ea1e62-a128-4139-a4f1-c9435edbf374",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "3c62039d-4810-4edc-884b-2aae2fa7d6bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "6e891065-5a1a-4041-9ca3-62d2a91aff99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c62039d-4810-4edc-884b-2aae2fa7d6bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf07194a-02be-4f3e-a8d2-e1d06a824490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c62039d-4810-4edc-884b-2aae2fa7d6bc",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "ffab4d50-77c0-4b86-8be5-2a12f133e713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "f32deb79-8727-474d-8ce5-d9b22f00b2da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffab4d50-77c0-4b86-8be5-2a12f133e713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553f1896-9696-43c5-b1fc-7b4891366e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffab4d50-77c0-4b86-8be5-2a12f133e713",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "876265c9-8c24-484a-94de-6796c07ecd07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "c5118d55-ed09-4516-9e0f-aac6c23a1c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "876265c9-8c24-484a-94de-6796c07ecd07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82698138-c630-406e-a459-9628f79e0881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "876265c9-8c24-484a-94de-6796c07ecd07",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "bf804a4c-afdd-4d09-9e21-afe82df650e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "c1dd6c26-926e-4ea2-a8a4-b57f0a8e8ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf804a4c-afdd-4d09-9e21-afe82df650e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5d30af-329b-4487-9281-ee11cafb08ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf804a4c-afdd-4d09-9e21-afe82df650e0",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "21fc3251-b055-4b3e-8b84-cd73afb7acbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "a642c031-0758-46d2-a8ab-89c9a1ce22de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fc3251-b055-4b3e-8b84-cd73afb7acbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82bac11b-9086-41e1-a4e9-916023079a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fc3251-b055-4b3e-8b84-cd73afb7acbf",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "9958e372-8b56-4c6e-b7dc-c6db2ed91f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "4ae40f97-572c-4ac1-a810-99418ccad8c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9958e372-8b56-4c6e-b7dc-c6db2ed91f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d51298a6-29a2-4c9c-a71e-1f7376f671aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9958e372-8b56-4c6e-b7dc-c6db2ed91f04",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "55b694d9-b223-485c-bfa8-14c00657fe05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "7786b648-39ec-4e8d-bc31-100353a8f00c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b694d9-b223-485c-bfa8-14c00657fe05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "428c8027-f7bd-44e7-a1c4-4f7a74e29f98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b694d9-b223-485c-bfa8-14c00657fe05",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "4ac89c01-a6c9-4c87-8957-aa78c4e7db53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "5f9cebb0-67dd-4f17-97dd-1cafb099de6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac89c01-a6c9-4c87-8957-aa78c4e7db53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131115c3-a1dd-4375-8763-94ef8dbb3556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac89c01-a6c9-4c87-8957-aa78c4e7db53",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "47ef884d-d0f6-4111-8b52-eeaac33c77dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "7752467f-e290-44c0-88f2-233fdcb26952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47ef884d-d0f6-4111-8b52-eeaac33c77dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6c471e8-ddcb-4284-8a0e-11366b2cf040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47ef884d-d0f6-4111-8b52-eeaac33c77dc",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "2109342c-1d18-4f75-80be-b21dfe5013e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "a0b8d1c3-4f2c-4ff4-a9a9-5e159b185275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2109342c-1d18-4f75-80be-b21dfe5013e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "458934cf-3e75-4f44-88a8-153fa104b346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2109342c-1d18-4f75-80be-b21dfe5013e5",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        },
        {
            "id": "9b46cdb9-6318-4154-a8b9-00a0dfdcc2be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "compositeImage": {
                "id": "6f21c8f1-4128-4f2a-b7fd-c6a194596ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b46cdb9-6318-4154-a8b9-00a0dfdcc2be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceeae740-eb37-4807-8127-b1154b55f50c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b46cdb9-6318-4154-a8b9-00a0dfdcc2be",
                    "LayerId": "95c05081-26d1-436e-a11d-21e563ac827f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "95c05081-26d1-436e-a11d-21e563ac827f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60ad6dbe-b636-4843-a69e-f7719de521aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 50,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 16
}