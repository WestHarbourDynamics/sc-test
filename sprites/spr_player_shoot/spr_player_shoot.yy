{
    "id": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 13,
    "bbox_right": 22,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11258cc6-0c6b-4ca2-9652-50c36574b5f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "ba0632c5-a699-4d4d-a462-158d3211ffed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11258cc6-0c6b-4ca2-9652-50c36574b5f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96586ac-2c31-44ec-b06a-001b34ad24f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11258cc6-0c6b-4ca2-9652-50c36574b5f1",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "fa5646c5-70c9-4b6f-b043-424eba0624db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11258cc6-0c6b-4ca2-9652-50c36574b5f1",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "228663fb-41f1-4f05-b1d1-dbc219125b29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "27674e03-681c-4487-824c-b6029647c857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228663fb-41f1-4f05-b1d1-dbc219125b29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c214f4-5181-466c-b361-18d7b57df54d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228663fb-41f1-4f05-b1d1-dbc219125b29",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "36ad74f8-a449-4ada-8734-285f98b9a198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228663fb-41f1-4f05-b1d1-dbc219125b29",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "ac55caf8-23e6-4aa1-abab-e984f383eb6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "3d24c0cf-f6a1-422c-8d41-aa4e57b6d3b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac55caf8-23e6-4aa1-abab-e984f383eb6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bf12aea-ea28-4586-a8d5-b976d947703e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac55caf8-23e6-4aa1-abab-e984f383eb6e",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "8682f529-ec82-42d8-acd0-d4ee778e557a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac55caf8-23e6-4aa1-abab-e984f383eb6e",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "8835063a-10dc-4ae8-9b1c-41824cb68a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "deebed3e-1db4-450a-b3a6-497c449e5869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8835063a-10dc-4ae8-9b1c-41824cb68a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0de1392-7973-44cd-83a6-46ea61f9ea9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8835063a-10dc-4ae8-9b1c-41824cb68a8c",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "6cbc8cdc-78b3-42df-bb13-9d9c19eedeab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8835063a-10dc-4ae8-9b1c-41824cb68a8c",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "b0779d16-6828-42be-bb2c-daee5eb8322a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "db9fce52-8da5-4d81-bab5-35c4b46f6403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0779d16-6828-42be-bb2c-daee5eb8322a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3940eab2-a9bc-46dc-9dbf-02a04f3a2ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0779d16-6828-42be-bb2c-daee5eb8322a",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "c4293182-f9f7-4d00-8fa8-b556f54ef73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0779d16-6828-42be-bb2c-daee5eb8322a",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "08191d3b-1074-4df7-9637-5af6c48891cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "88f7854e-1fbf-448e-9902-8c4b6dccc8de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08191d3b-1074-4df7-9637-5af6c48891cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b260483a-6c9e-443c-8cd9-7d80a2b3114a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08191d3b-1074-4df7-9637-5af6c48891cc",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "854d8cf8-1903-4356-bf39-b35b668c8b39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08191d3b-1074-4df7-9637-5af6c48891cc",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "b4395568-721a-4a78-9633-5b8333a06c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "53c253e7-cc62-471a-96dd-c46436a28fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4395568-721a-4a78-9633-5b8333a06c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd374f8-5e06-42c7-be48-449d2bb32a2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4395568-721a-4a78-9633-5b8333a06c05",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "455a7d5f-28d3-4390-8384-435a5754541f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4395568-721a-4a78-9633-5b8333a06c05",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "21f83a25-10d6-455c-ab2b-e56cd9236470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "5ca6a4ba-0964-4ba5-b731-ec74cf28418c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f83a25-10d6-455c-ab2b-e56cd9236470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d224b3ac-d4aa-484e-9828-57b55a7babed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f83a25-10d6-455c-ab2b-e56cd9236470",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "a31687ea-c98c-409b-b35d-a8c6b66d6b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f83a25-10d6-455c-ab2b-e56cd9236470",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "123ae35e-667b-4d38-a559-e2393b5690ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "b3646705-84be-440b-9e84-5c52545e1c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123ae35e-667b-4d38-a559-e2393b5690ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d1fb3fe-399f-4f17-a494-f88596590fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123ae35e-667b-4d38-a559-e2393b5690ad",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "6f2d4371-9645-449b-9c38-85f6cc547e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123ae35e-667b-4d38-a559-e2393b5690ad",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "47f4ae1d-a316-40fb-924c-521a8d32cd87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "6dfdd2f8-2285-4d28-bece-30a23dd7d46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f4ae1d-a316-40fb-924c-521a8d32cd87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78bbf71-69a4-45e3-918e-a0ade5fd5363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f4ae1d-a316-40fb-924c-521a8d32cd87",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "4bc55f86-831d-4d35-9226-ec77da2502b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f4ae1d-a316-40fb-924c-521a8d32cd87",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "51038e09-3fd3-4dff-95f8-bdf94ac7b290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "ecc56c19-a620-48d2-b63f-19b1a3b04f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51038e09-3fd3-4dff-95f8-bdf94ac7b290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e179db9b-e202-4c2e-94a2-4351d5e8fda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51038e09-3fd3-4dff-95f8-bdf94ac7b290",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "7de3b4ee-a8e5-4b70-b60d-c214d7c24842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51038e09-3fd3-4dff-95f8-bdf94ac7b290",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "35fbe347-b8b8-45d8-95dc-e489a528c4fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "8f0656f5-6e7b-4b9d-8910-37b7f637ae51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35fbe347-b8b8-45d8-95dc-e489a528c4fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdbf7e26-ab97-407e-9d11-66c4ce16bb7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35fbe347-b8b8-45d8-95dc-e489a528c4fe",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "2f39cae9-4b52-4958-b048-7c234ce81c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35fbe347-b8b8-45d8-95dc-e489a528c4fe",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "d8b4efd9-42c8-4554-99cb-39d167e1b803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "f8b71781-93c1-4ddb-bf75-298e871cbece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b4efd9-42c8-4554-99cb-39d167e1b803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d802a63c-dfaf-48f2-8312-cca2259f70e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b4efd9-42c8-4554-99cb-39d167e1b803",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "8c85012d-d058-4887-b7a7-434248d107f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b4efd9-42c8-4554-99cb-39d167e1b803",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "2a9f250c-b02f-4c9a-acdb-a624b360c566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "7f9d79e6-de92-4603-b970-80ac91ef1934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a9f250c-b02f-4c9a-acdb-a624b360c566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb707f4-7852-4c7c-b490-49f0747701d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9f250c-b02f-4c9a-acdb-a624b360c566",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "10c38388-d050-4885-8dce-17db0fdfd96b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9f250c-b02f-4c9a-acdb-a624b360c566",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "69f14519-db9f-4e5f-91ae-ccf767705416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "2a054a11-224d-4b2c-9213-15f340113f1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f14519-db9f-4e5f-91ae-ccf767705416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7f5d07-631d-42a9-ba7e-9d435a3dfec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f14519-db9f-4e5f-91ae-ccf767705416",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "0aaae383-30b6-4c23-a48a-250de40be8c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f14519-db9f-4e5f-91ae-ccf767705416",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "740c9c0d-0779-4135-82f3-d4ab40a6fb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "c758a144-57dc-4291-b9a1-06e41b625c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740c9c0d-0779-4135-82f3-d4ab40a6fb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fc76f6e-1762-4208-80d4-d8a1c2aa0f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740c9c0d-0779-4135-82f3-d4ab40a6fb2e",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "84e3cc2d-f477-45bb-8419-a993ceeeaa34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740c9c0d-0779-4135-82f3-d4ab40a6fb2e",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "14971cfd-5cc0-4a08-96e3-55ce5fdb0dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "334e7416-c8b8-43b2-827e-edda8c11f23e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14971cfd-5cc0-4a08-96e3-55ce5fdb0dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5069b65a-9b03-4ea2-8db2-ac4784df2184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14971cfd-5cc0-4a08-96e3-55ce5fdb0dc9",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "459ae156-9486-4cd6-9715-3c42d7bd302c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14971cfd-5cc0-4a08-96e3-55ce5fdb0dc9",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "ce3814b2-f79d-49cd-bbd2-60ab3dee1593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "9589f2be-c70c-4350-a26d-0bc3aeea34d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce3814b2-f79d-49cd-bbd2-60ab3dee1593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bceea1-a8d4-4b47-a96a-d8ad6e6cca8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3814b2-f79d-49cd-bbd2-60ab3dee1593",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "16632091-8b8c-46d1-9a0c-88fe578e8634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3814b2-f79d-49cd-bbd2-60ab3dee1593",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "fc3c906f-b1ff-480c-bba2-d896e51ea201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "30eb2e8f-99a2-4bf9-a6b2-0895625f0c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3c906f-b1ff-480c-bba2-d896e51ea201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "841ccbcb-ea1a-4910-ba0e-a32b2f82daa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3c906f-b1ff-480c-bba2-d896e51ea201",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "037ed30f-f5e3-4a0e-8fa4-3a42017c7cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3c906f-b1ff-480c-bba2-d896e51ea201",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "81677da4-d138-48ce-86c7-4e101f7067f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "fc31bbd0-0edc-4663-8f01-9b0aa056898e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81677da4-d138-48ce-86c7-4e101f7067f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea900a0-7630-4180-94b9-490162c3b01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81677da4-d138-48ce-86c7-4e101f7067f3",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "61f523b2-6bae-4443-b99b-5cf62417d234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81677da4-d138-48ce-86c7-4e101f7067f3",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "37b476a1-1701-4ad5-9e04-f5284380cf37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "d444eb34-673a-43e0-aaf6-844c12579af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b476a1-1701-4ad5-9e04-f5284380cf37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22c963b-ee08-4ee1-8676-8b45eccb01f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b476a1-1701-4ad5-9e04-f5284380cf37",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "78b3c99a-f459-406a-bb34-fbb447fd42db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b476a1-1701-4ad5-9e04-f5284380cf37",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "fc38e1fe-d1a4-40e1-9ebc-be6e7fc7298e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "aa777fd9-dde3-438d-b4ee-a4877f15251d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc38e1fe-d1a4-40e1-9ebc-be6e7fc7298e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939eb0ae-4785-4810-b7bd-d8517d4eecd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc38e1fe-d1a4-40e1-9ebc-be6e7fc7298e",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "bc03c554-6b9e-4e26-9fee-c148b8612917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc38e1fe-d1a4-40e1-9ebc-be6e7fc7298e",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        },
        {
            "id": "69129eb2-0edd-4112-bed0-ad42b0ec5d45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "compositeImage": {
                "id": "17ab237b-a495-43da-9681-08c4cb141f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69129eb2-0edd-4112-bed0-ad42b0ec5d45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ccf23fe-7371-4136-9112-9fbc3a36370f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69129eb2-0edd-4112-bed0-ad42b0ec5d45",
                    "LayerId": "47a215db-ec7a-4cb2-b3dd-7b619a74d277"
                },
                {
                    "id": "44082f98-daae-439b-b3f2-25f565d3cd6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69129eb2-0edd-4112-bed0-ad42b0ec5d45",
                    "LayerId": "675852d5-50b5-4d7a-8215-15135eac72a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "675852d5-50b5-4d7a-8215-15135eac72a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "47a215db-ec7a-4cb2-b3dd-7b619a74d277",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77728b45-6fbb-4e5c-aeb5-f24a76cf85d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 50,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 16
}