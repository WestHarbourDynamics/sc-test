{
    "id": "e1a9aa95-f209-4d85-8d64-9987709e737c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28a2bcc7-aad2-4e49-90c6-ae5684c9bc9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1a9aa95-f209-4d85-8d64-9987709e737c",
            "compositeImage": {
                "id": "aab16eb2-9d25-4588-b200-30d19d0731b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a2bcc7-aad2-4e49-90c6-ae5684c9bc9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebdf2642-df1b-47d8-b2b6-22c943b8823a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a2bcc7-aad2-4e49-90c6-ae5684c9bc9e",
                    "LayerId": "947c5a4f-91b6-4b37-ba87-036e1896990a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "947c5a4f-91b6-4b37-ba87-036e1896990a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1a9aa95-f209-4d85-8d64-9987709e737c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}