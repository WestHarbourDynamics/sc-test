{
    "id": "7f1d4e8c-95e6-45c5-8483-6ecbb9907628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_auto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1dfad89-12d2-474a-b47d-c3dd7408dde7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f1d4e8c-95e6-45c5-8483-6ecbb9907628",
            "compositeImage": {
                "id": "6ddc5db4-f1d2-4ef9-bf3b-445ff5ef58cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1dfad89-12d2-474a-b47d-c3dd7408dde7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb89efd8-85ec-4c58-8af8-cb0f83d26964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1dfad89-12d2-474a-b47d-c3dd7408dde7",
                    "LayerId": "516ecc9c-bd0b-4d89-82c3-8b26e7da4a03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "516ecc9c-bd0b-4d89-82c3-8b26e7da4a03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f1d4e8c-95e6-45c5-8483-6ecbb9907628",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}