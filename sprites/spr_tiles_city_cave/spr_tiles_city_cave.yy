{
    "id": "fc293b66-e0c4-4394-a0c3-ac0e92a4226d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_city_cave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1631,
    "bbox_left": 0,
    "bbox_right": 607,
    "bbox_top": 192,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9cae444-7b0d-4801-ac03-c23b2ae27927",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc293b66-e0c4-4394-a0c3-ac0e92a4226d",
            "compositeImage": {
                "id": "361d2746-6d36-4ffd-8fc9-24ef692d5af7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9cae444-7b0d-4801-ac03-c23b2ae27927",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e26f83-9396-4dae-a77c-b6e14aadfd86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9cae444-7b0d-4801-ac03-c23b2ae27927",
                    "LayerId": "671c2eea-ef28-4a39-9e0b-9384a7f8e422"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1680,
    "layers": [
        {
            "id": "671c2eea-ef28-4a39-9e0b-9384a7f8e422",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc293b66-e0c4-4394-a0c3-ac0e92a4226d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 608,
    "xorig": 0,
    "yorig": 0
}