{
    "id": "ecc1bab1-2782-49ab-a329-42b425864beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a198d0b6-7b53-4b30-95ff-91b4b22d0154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecc1bab1-2782-49ab-a329-42b425864beb",
            "compositeImage": {
                "id": "1b2071a4-5573-4b17-b0bd-e758f62d90e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a198d0b6-7b53-4b30-95ff-91b4b22d0154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab2ca03-4d65-4c7d-8f46-24ddc5b18155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a198d0b6-7b53-4b30-95ff-91b4b22d0154",
                    "LayerId": "bb6fb23e-2e3e-4d63-8442-7c0d62eff061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bb6fb23e-2e3e-4d63-8442-7c0d62eff061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecc1bab1-2782-49ab-a329-42b425864beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}