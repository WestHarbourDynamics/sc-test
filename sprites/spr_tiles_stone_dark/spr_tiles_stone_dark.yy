{
    "id": "02f99641-fa61-48d4-836f-497117fa9ecc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_stone_dark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 29,
    "bbox_right": 607,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7866523a-2d74-42da-8181-c710fddd7f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02f99641-fa61-48d4-836f-497117fa9ecc",
            "compositeImage": {
                "id": "f8a0d19d-9976-48e9-b645-06afddfed161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7866523a-2d74-42da-8181-c710fddd7f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b27d72-c261-4103-8c01-67320b4964ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7866523a-2d74-42da-8181-c710fddd7f21",
                    "LayerId": "5a395f4f-04a2-434d-a7d5-0ea50111e9a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "5a395f4f-04a2-434d-a7d5-0ea50111e9a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02f99641-fa61-48d4-836f-497117fa9ecc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}