{
    "id": "ff3f2037-a8fa-481a-b048-ba0af3dfcd1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_stone_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 29,
    "bbox_right": 607,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3810b9bf-3068-4102-9c05-815bce43bc28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3f2037-a8fa-481a-b048-ba0af3dfcd1b",
            "compositeImage": {
                "id": "86ed7928-6023-48ee-9781-c5a9008b83ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3810b9bf-3068-4102-9c05-815bce43bc28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d646bb-ac9e-45e7-8b9d-57eb28ee1d18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3810b9bf-3068-4102-9c05-815bce43bc28",
                    "LayerId": "50b2a156-05b5-4177-a09d-bf0f8cb964ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "50b2a156-05b5-4177-a09d-bf0f8cb964ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff3f2037-a8fa-481a-b048-ba0af3dfcd1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}