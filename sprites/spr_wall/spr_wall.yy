{
    "id": "f5fbe15a-8456-47f1-a0f1-65fc8395c773",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87b850b6-a674-4323-bcd6-b38cab376981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5fbe15a-8456-47f1-a0f1-65fc8395c773",
            "compositeImage": {
                "id": "e438726e-954a-488b-8645-c4a16298ff8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b850b6-a674-4323-bcd6-b38cab376981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77bfc8f3-a8d0-4a71-b186-36ea842abc71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b850b6-a674-4323-bcd6-b38cab376981",
                    "LayerId": "46434106-c6f2-46d6-a933-a730d22a72ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46434106-c6f2-46d6-a933-a730d22a72ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5fbe15a-8456-47f1-a0f1-65fc8395c773",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}